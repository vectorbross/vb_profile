<?php

/**
 * @file
 * Enables modules and site configuration for the Vector bross Profile.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\FileStorage;
use Drupal\views\Views;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\webform\Entity\Webform;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\vb_profile\Form\AssemblerForm;
use \Drupal\vb_profile\Form\ChoosePlanForm;
use Drupal\vb_profile\Form\ConfigureMultilingualForm;

/**
 * Implements hook_install_tasks_alter().
 */
function vb_profile_install_tasks_alter(&$tasks, $install_state) {
  // Add our own wrapper to install_profile_modules to suppress messages.
  // TODO: needs to be reworked for D9 - if still necessary at all
  //$tasks['install_profile_modules']['function'] = 'vb_profile_install_profile_modules';

  $tasks['install_finished']['function'] = 'vb_profile_after_install_finished';
}

/**
 * Implements hook_install_tasks().
 */
function vb_profile_install_tasks(&$install_state) {

  // Determine whether the enable multilingual option is selected during the
  // Multilingual configuration task.
  $needs_configure_multilingual = (isset($install_state['vb_profile']['enable_multilingual']) && $install_state['vb_profile']['enable_multilingual'] == TRUE);

  return [
    'vb_profile_choose_install_flow' => [
      'display_name' => t('Choose Plan (Fix/Flex)'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ChoosePlanForm::class,
    ],
    'vb_profile_multilingual_configuration_form' => [
      'display_name' => t('Multilingual configuration'),
      'display' => TRUE,
      'type' => 'form',
      'function' => ConfigureMultilingualForm::class,
    ],
    'vb_profile_configure_multilingual' => [
      'display_name' => t('Configure multilingual'),
      'display' => $needs_configure_multilingual,
      'type' => 'batch',
    ],
    'vb_profile_extra_components' => [
      'display_name' => t('Extra components'),
      'display' => TRUE,
      'type' => 'form',
      'function' => AssemblerForm::class,
    ],
    'vb_profile_assemble_extra_components' => [
      'display_name' => t('Assemble extra components'),
      'display' => TRUE,
      'type' => 'batch',
    ],
    'vb_profile_set_custom_default_permissions' => [
      'display_name' => t('Set default permissions'),
      'display' => TRUE,
      'type' => 'batch',
    ],
    'vb_profile_set_client_information' => [
      'display_name' => t('Client information'),
      'display' => TRUE,
      'type' => 'form',
      'function' => \Drupal\vb_core\Form\SiteInfoConfigForm::class,
    ],
  ];
}

/**
 * Install modules.
 *
 * Wrapper around install_profile_modules to add a hide warning and status
 * message operation at the end. We don't need to see any module info
 * messages during site installation.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   Return batch array.
 */
function vb_profile_install_profile_modules(array &$install_state) {
  $batch = vb_install_profile_modules($install_state);
  // Add hide message as last batch.
  $batch['operations'][] = [
    'vb_profile_postpone_messages',
    (array) TRUE,
  ];

  return $batch;
}

/**
 * Installs required modules via a batch process.
 *
 * @param $install_state
 *   An array of information about the current installation state.
 *
 * @return
 *   The batch definition.
 */
function vb_install_profile_modules(&$install_state) {
  // We need to manually trigger the installation of core-provided entity types,
  // as those will not be handled by the module installer.
  install_core_entity_type_definitions();

  $modules = \Drupal::state()->get('install_profile_modules') ?: [];
  $files = \Drupal::service('extension.list.module')->getList();
  \Drupal::state()->delete('install_profile_modules');

  // Always install required modules first. Respect the dependencies between
  // the modules.
  $required = [];
  $non_required = [];

  // Add modules that other modules depend on.
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      $modules = array_merge($modules, array_keys($files[$module]->requires));
    }
  }
  $modules = array_unique($modules);
  foreach ($modules as $module) {
    if (!empty($files[$module]->info['required'])) {
      $required[$module] = $files[$module]->sort;
    }
    else {
      $non_required[$module] = $files[$module]->sort;
    }
  }
  arsort($required);
  arsort($non_required);

  $operations = [];
  foreach ($required + $non_required as $module => $weight) {
    $operations[] = ['_vb_install_module_batch', [$module, $files[$module]->info['name']]];
  }
  $batch = [
    'operations' => $operations,
    'title' => t('Installing @drupal', ['@drupal' => drupal_install_profile_distribution_name()]),
    'error_message' => t('The installation has encountered an error.'),
  ];
  return $batch;
}

/**
 * Implements callback_batch_operation().
 *
 * Performs batch installation of modules.
 */
function _vb_install_module_batch($module, $module_name, &$context) {
  //drupal_flush_all_caches();
  \Drupal::service('module_installer')->install([$module], TRUE);
  $context['results'][] = $module;
  $context['message'] = t('Installed %module module.', ['%module' => $module_name]);
}

/**
 * Batch job to configure multilingual components.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   The batch job definition.
 */
function vb_profile_configure_multilingual(array &$install_state) {
  $batch = [];

  // If the multilingual config checkbox was checked.
  if (!empty($install_state['vb_profile']['enable_multilingual'])) {
    // Add all selected languages.
    foreach ($install_state['vb_profile']['multilingual_languages'] as $language_code) {
      $batch['operations'][] = [
        'vb_profile_enable_language',
        (array) $language_code,
      ];
    }

    // Enable content_translation module.
    \Drupal::service('module_installer')->install(['content_translation']);

    // Hide warnings and status messages.
    $batch['operations'][] = [
      'vb_profile_postpone_messages',
      (array) TRUE,
    ];

  }

  // Fix entity updates to clear up any mismatched entity.
  $batch['operations'][] = [
    'vb_profile_fix_entity_update',
    (array) TRUE,
  ];

  return $batch;
}

/**
 * Batch function to hide warning messages.
 */
function vb_profile_postpone_messages() {
  global $_SESSION;

  $messenger = \Drupal::messenger();
  $messages = $messenger->all();
  $messenger->deleteAll();

  if (!isset($_SESSION['install_state']['vb_profile']['saved_messages'])) {
    $_SESSION['install_state']['vb_profile']['saved_messages'] = [];
  }

  // Save all messages to output them at the end.
  foreach ($messages as $type => $list) {
    foreach ($list as $idx => $message) {
      $needles = [
        'This site has only a single language enabled',
        'Enable translation for content types',
      ];
      foreach ($needles as $needle) {
        $message = strip_tags((string) $message);
        if (strpos($message, $needle) === 0) {
          unset($list[$idx]);
        }
      }
    }
    if (!isset($_SESSION['install_state']['vb_profile']['saved_messages'][$type])) {
      $_SESSION['install_state']['vb_profile']['saved_messages'][$type] = [];
    }
    $_SESSION['install_state']['vb_profile']['saved_messages'][$type] = array_merge($_SESSION['install_state']['vb_profile']['saved_messages'][$type], $list);
  }
}

/**
 * Batch function to assemble and install needed extra components.
 *
 * @param string|array $extra_component
 *   Name of the extra component.
 */
function vb_profile_assemble_extra_component_then_install($extra_component) {
  \Drupal::service('module_installer')->install((array) $extra_component, TRUE);
}

/**
 * Batch function to add selected languages then fetch all translations.
 *
 * @param string|array $language_code
 *   Language code to install and fetch all traslation.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function vb_profile_enable_language($language_code) {
  ConfigurableLanguage::createFromLangcode($language_code)->save();
}

/**
 * Batch function to fix entity updates to clear up any mismatched entity.
 *
 * Entity and/or field definitions, The following changes were detected in
 * the entity type and field definitions.
 *
 * @param string|array $entity_update
 *   To entity update or not.
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function vb_profile_fix_entity_update($entity_update) {
  // Removed call to deprecated function. Should no longer be needed either.
}

/**
 * Batch job to assemble vb_profile extra components.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   The batch job definition.
 */
function vb_profile_assemble_extra_components(array &$install_state) {
  drupal_flush_all_caches();
  // Get extra vb-modules to enable.
  $modules = $install_state['vb_profile']['extra_features'] ?: [];

  // Get default content modules to enable.
  $default_content = [];
  $modules_list = \Drupal::service('extension.list.module')->reset()->getList();
  if (array_key_exists('vb_core_default_content', $modules_list)) {
    //$default_content[] = 'vb_core_default_content';
  }

  // Also chuck in any modules required by the selected theme.
  $themes = \Drupal::service('extension.list.theme')->reset()->getList();
  $theme = $themes[$GLOBALS['install_state']['vb_profile']['theme']];
  $requirements = !empty($theme->info['requirements']) ? $theme->info['requirements'] : [];
  $modules = array_merge($modules, $requirements);

  // Append default content to install to modules-array.
  $modules = array_merge($modules, $default_content);

  $batch = _vb_profile_assemble_extra_components_batch($modules);

  // After enabling the modules and default-content, we can disable the default-content modules again.
  // We pass all default-content modules.
  $default_content += $install_state['vb_profile']['default_content'] ?: [];

  $batch['operations'][] = [
    'vb_profile_disable_default_content_modules',
    $default_content,
  ];

  // Enable the theme as last.
  // If we have a custom subtheme, enable that one.
  if (isset($GLOBALS['install_state']['vb_profile']['new_theme_name'])) {
    $batch['operations'][] = [
      'vb_profile_install_theme',
      [$GLOBALS['install_state']['vb_profile']['new_theme_name']],
    ];
  }
  else {
    // Enable default theme.
    $batch['operations'][] = [
      'vb_profile_install_theme',
      [$GLOBALS['install_state']['vb_profile']['theme']],
    ];
  }
  // Hide warnings and status messages.
  $batch['operations'][] = [
    'vb_profile_postpone_messages',
    (array) TRUE,
  ];

  return $batch;
}

/**
 * Batch job to set our own default permissions.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   The batch job definition.
 */
function vb_profile_set_custom_default_permissions(array &$install_state) {
  drupal_flush_all_caches();

  // Get all enabled modules.
  $modules_list = \Drupal::service('extension.list.module')->reset()->getList();

  foreach ($modules_list as $name => $module) {
    if (function_exists($name . '_set_default_vb_permissions')) {
      $function = $name . '_set_default_vb_permissions';
      $function();
    }
  }

  return [];
}

/**
 * Installs the chosen theme.
 *
 * @param string $theme
 *   Theme name.
 *
 */
function vb_profile_install_theme($theme) {
  \Drupal::service('theme_installer')->install([$theme]);

  // Also set it as the default theme.
  \Drupal::configFactory()->getEditable('system.theme')
    ->set('default', $theme)
    ->save();
}

/**
 * Disables a given list of modules.
 *
 * @param $modules
 */
function vb_profile_disable_default_content_modules($modules) {
  foreach ($modules as $name => $status) {
    \Drupal::service('module_installer')->uninstall([$name], FALSE);
  }
}

/**
 * Create batch array for list of modules to be installed.
 *
 * @param array $modules
 *   List of modules.
 *
 * @return array
 *   Batch array.
 */
function _vb_profile_assemble_extra_components_batch(array $modules) {
  $files = \Drupal::service('extension.list.module')->reset()->getList();

  // Always install required modules first. Respect the dependencies between
  // the modules.
  $required = [];
  $non_required = [];

  if ($key = array_search('new_theme_name', $modules)) {
    unset($modules[$key]);
  }
  if ($key = array_search('fix_theme_name', $modules)) {
    unset($modules[$key]);
  }
  // Add modules that other modules depend on.
  foreach ($modules as $module) {
    if ($files[$module]->requires) {
      $modules = array_merge($modules, array_keys($files[$module]->requires));
    }
  }
  $modules = array_unique($modules);
  foreach ($modules as $module) {
    if (!empty($files[$module]->info['required'])) {
      $required[$module] = $files[$module]->sort;
    }
    else {
      if(isset($files[$module]->sort)) {
        $non_required[$module] = $files[$module]->sort;
      }
    }
  }
  arsort($required);
  arsort($non_required);

  $operations = [];
  foreach ($required + $non_required as $module => $weight) {
    $operations[] = [
      '_install_module_batch',
      [$module, $files[$module]->info['name']],
    ];
  }
  $batch = [
    'operations' => $operations,
    'title' => t('Installing @drupal', ['@drupal' => drupal_install_profile_distribution_name()]),
    'error_message' => t('The installation has encountered an error.'),
  ];
  // Hide warnings and status messages.
  $batch['operations'][] = [
    'vb_profile_postpone_messages',
    (array) TRUE,
  ];

  return $batch;
}

/**
 * Runs after install is finished.
 *
 * @param array $install_state
 *   The current install state.
 *
 * @return array
 *   Renderable array to output.
 */
function vb_profile_after_install_finished(array &$install_state) {
  global $_SESSION;

  install_finished($install_state);

  // Rebuild permissions.
  node_access_rebuild();

  // Delete "Contact" webform.
  try {
    $form = Webform::load('contact');
    if ($form) {
      $form->delete();
    }
  }
  catch (EntityStorageException $e) {
    // No biggie.
  }

  try {
    // Delete frontpage view.
    $view = Views::getView('frontpage');
    $view->destroy();
    $view->storage->delete();
  }
  catch (\Exception $e) {
    // No biggie. Still part of checklist.
  }

  // Delete the congratulations message
  // and node rebuild message.
  $messenger = \Drupal::messenger();
  $messenger->deleteAll();

  $output = [
    '#title' => t('Installation finished'),
    'info' => [
      '#type' => 'container',
      'congratulations' => [
        '#markup' => new FormattableMarkup('<p>' .
          t('Congratulations, you have successfully installed Vector bross Profile') .
          '</p>', []),
      ],
      'drush_info' => [
        '#markup' => new FormattableMarkup('<p><strong style="background-color: #ffa500">' .
          t('If you wish to fully setup your local environment, please run the following drush command from just inside the docroot. It will setup all the required config split folders and populate them.') .
          '</strong></p>', []),
      ],
      'drush' => [
        '#markup' => new FormattableMarkup('<pre><code style="background-color: #444040; color: #ffffff;display:block;padding:20px;">drush d-set</code></pre>', []),
      ],
    ],
    'messages' => [
      '#type' => 'details',
      '#title' => t('Messages'),
      '#description' => t('Messages output during install'),
    ],
    'visit_site' => [
      '#markup' => '<a href="/">' . t('Visit your website') . '</a>',
    ],
  ];

  foreach ($_SESSION['install_state']['vb_profile']['saved_messages'] as $type => $messages) {

    $output['messages'][$type] = [
      '#theme' => 'item_list',
      '#title' => t('Type: @type', ['@type' => $type]),
      '#list_type' => 'ul',
      '#items' => [],
      '#attributes' => [
        'class' => ['color-' . $type],
        'style' => 'margin-bottom:15px;',
      ],
    ];

    foreach ($messages as $message) {
      // For some reason <front> turns into /core/install.php during
      // installation so replace that part and reinsert into
      // FormattableMarkup else it'll escape any HTML.
      $message = str_replace('/core/install.php', '', (string) $message);
      $output['messages'][$type]['#items'][] =
        new FormattableMarkup($message, []);

      if (_vb_profile_is_drupal_cli()) {
        // Re-add the messages for CLI.
        $messenger->addMessage($message, $type);
      }
    }
  }

  unset($_SESSION['install_state']['vb_profile']);

  if (_vb_profile_is_drupal_cli()) {
    $messenger->addMessage(t('If you wish to fully setup your local environment, please run the drush command "d-set" which will create and populate all the config split folders'), 'warning');
  }

  // Clear all caches. Mostly because deleting frontpage /node
  // view breaks that path until caches are cleared.
  drupal_flush_all_caches();

  return $output;
}

/**
 * Check if Drupal is running in CLI.
 *
 * @see https://www.drupal.org/project/drupal/issues/2904700
 *
 * @return bool
 *   If Drupal is running in CLI.
 */
function _vb_profile_is_drupal_cli() {
  if (defined('STDIN')) {
    return TRUE;
  }

  if (in_array(PHP_SAPI, ['cli', 'cli-server', 'phpdbg'])) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_themes_installed().
 */
function vb_profile_themes_installed($themes) {
  // Check if post-install config should be imported.
  foreach ($themes as $theme) {
    // Install extra VB Fix config after install of custom theme.
    $config_files_path = \Drupal::service('extension.list.theme')->getPath($theme) . '/config/post_install';

    $config_source = new FileStorage($config_files_path);
    \Drupal::service('config.installer')->installOptionalConfig($config_source);
  }
}
