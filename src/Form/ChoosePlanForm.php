<?php

namespace Drupal\vb_profile\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ChoosePlanForm class.
 *
 * Defines form for selecting vb_profile's plan (Fix or Flex).
 */
class ChoosePlanForm extends FormBase {

  /**
   * Configure Multilingual Form constructor.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translator
   *   The string translation service.
   */
  public function __construct(TranslationInterface $translator) {
    $this->stringTranslation = $translator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_profile_choose_plan';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, array &$install_state = NULL) {
    $form['#title'] = $this->t('Choose your plan');

    $bolt_plan_options = [
      'fix' => $this->t('Bolt Fix'),
      'flex' => $this->t('Bolt Flex'),
    ];

    $themes = \Drupal::service('extension.list.theme')->reset()->getList();
    // If no Bolt fix theme available, unset from options.
    if (!array_key_exists('vb_subtheme_fix', $themes)) {
      unset($bolt_plan_options['fix']);
    }

    $form['bolt_plan'] = [
      '#type' => 'select',
      '#title' => $this->t('Please select your plan'),
      '#required' => TRUE,
      '#options' => $bolt_plan_options,
    ];

    $form['actions'] = [
      'continue' => [
        '#type' => 'submit',
        '#value' => $this->t('Save and continue'),
        '#button_type' => 'primary',
      ],
      '#type' => 'actions',
      '#weight' => 5,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::keyValue('vb_profile')->set('bolt_plan', $form_state->getValue('bolt_plan'));
  }

}
