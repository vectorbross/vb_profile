<?php

namespace Drupal\vb_profile\Form;

use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines form for selecting extra components for the assembler to install.
 */
class AssemblerForm extends FormBase {

  const MODULE_PACKAGE_NAME = 'Vector BROSS';

  const THEME_PACKAGE_NAME = 'Vector BROSS';

  /**
   * The info parser service.
   *
   * @var \Drupal\Core\Extension\InfoParserInterface
   */
  protected $infoParser;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Assembler Form constructor.
   *
   * @param \Drupal\Core\Extension\InfoParserInterface $info_parser
   *   The info parser service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translator
   *   The string translation service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The Module Handler service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The Theme Handler service.
   */
  public function __construct(InfoParserInterface $info_parser, TranslationInterface $translator, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $themeHandler) {
    $this->infoParser = $info_parser;
    $this->stringTranslation = $translator;
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $themeHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('info_parser'),
      $container->get('string_translation'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_profile_extra_components';
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   Extra components modules.
   */
  public function buildForm(array $form, FormStateInterface $form_state, array &$install_state = NULL) {
    $form['#title'] = $this->t('Extra components');
    $form['extra_components_introduction'] = [
      '#weight' => -999,
      '#prefix' => '<p>',
      '#markup' => $this->t('Install additional ready-to-use components for your site.'),
      '#suffix' => '</p>',
    ];

    // Include system.admin.inc so we can use the sort callbacks.
    $this
      ->moduleHandler
      ->loadInclude('system', 'inc', 'system.admin');
    // Sort all modules by their names.
    $modules = \Drupal::service('extension.list.module')->reset()->getList();
    uasort($modules, [ModuleExtensionList::class, 'sortByName']);

    // Set up features.
    foreach ($modules as $filename => $module) {
      // Grab all modules that should be shown here.
      if (empty($module->info['show_during_install'])) {
        continue;
      }
      // Make a fieldset wrapper, NO TREE!
      $form[$filename . '_wrapper'] = [
        '#type' => 'fieldset',
        '#title' => $module->info['name'],
        '#tree' => FALSE,
        '#weight' => $module->info['weight'] ?? 0,
      ];

      // Add a link to the FA if available.
      if (!empty($module->info['fa_link'])) {
        $form[$filename . '_wrapper']['extra_info_wrapper'] = [
          '#type' => 'container',
          '#attributes' => [
            'style' => 'display:block;margin-top:5px;',
          ],
        ];

        $form[$filename . '_wrapper']['extra_info_wrapper']['extra_info'] = [
          '#title' => $this->t('Functional Analysis'),
          '#type' => 'link',
          '#url' => Url::fromUri($module->info['fa_link'], ['attributes' => ['target' => '_blank']]),
        ];
      }

      // Add the module itself as the first module.
      $form[$filename . '_wrapper']['extra_features'][$filename] = [
        '#type' => 'checkbox',
        '#title' =>  isset($module->info['name']) ? $this->t('Enable') . ' ' . $module->info['name'] : $this->t('Core'),
        '#description' => $this->t($module->info['description']),
        '#default_value' => (bool) empty($module->info['prechecked']) ? $module->status : $module->info['prechecked'],
        // If already enabled at this point, don't allow them to uncheck it
        // Means it's a dependency of the profile and they can't not install
        // it anyway.
        '#disabled' => (bool) $module->status,
      ];

      // Check if default content exists for module and add checkbox to install default content.
      if (array_key_exists($filename . '_default_content', $modules)) {
        $default_content_module = $modules[$filename . '_default_content'];

        $form[$filename . '_wrapper']['extra_features'][$filename . '_default_content'] = [
          '#type' => 'checkbox',
          '#title' =>  isset($default_content_module->info['name']) ? $this->t('Install') . ' ' . $default_content_module->info['name'] : $this->t('Core'),
          '#description' => $this->t($default_content_module->info['description']),
          '#default_value' => (bool) empty($default_content_module->info['prechecked']) ? $default_content_module->status : $default_content_module->info['prechecked'],
          '#states' => [
            'visible' => [
              ':input[name="' . $filename . '_wrapper[extra_features][' . $filename . ']"]' => [
                'checked' => TRUE,
              ],
            ],
          ],
        ];
      }

      // Add states if present.
      if (!empty($module->info['states'])) {
        $form[$filename . '_wrapper']['extra_features'][$filename]['#states']
          = $module->info['states'];
      }
      // Add any upgrades defined in the info.yml.
      if (!empty($module->info['upgrades'])) {
        foreach ($module->info['upgrades'] as $upgrade) {
          list($file, $name) = explode(':', $upgrade);
          $info = $modules[$file];
          $form[$filename . '_wrapper']['extra_features'][$file] = [
            '#type' => 'checkbox',
            '#title' => $this->t($name),
            '#description' => $this->t($info->info['description']),
            '#default_value' => (bool) empty($info->info['prechecked']) ? $info->status : $info->info['prechecked'],
            '#disabled' => (bool) $info->status,
          ];
          // Add states to these upgrades if defined.
          if (!empty($info->info['states'])) {
            $form[$filename . '_wrapper']['extra_features'][$file]['#states']
              = $info->info['states'];
          }
        }
      }
    }

    $form['theme_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Site theme'),
      '#tree' => FALSE,
      '#weight' => 50,
    ];

    // Check Bolt Plan. If Fix, don't allow choosing theme. Only set theme machine name, which will be an empty
    //subtheme of the Bolt Fix theme (demo-site).
    $bolt_plan = \Drupal::keyValue('vb_profile')->get('bolt_plan');

    if (isset($bolt_plan) && $bolt_plan == 'fix') {
      // We will create an empty subtheme.
      $form['theme_wrapper']['fix_theme_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Bolt Fix theme machine name'),
        '#description' => $this->t('Please provide a machine-name for the Bolt Fix theme.'),
        '#machine_name' => [
          'replace_pattern' => '[^a-z_]+',
          'replace' => '_',
        ],
        '#element_validate' => [
          [static::class, 'validateMachineName'],
        ],

      ];
    }
    else {
      $form['theme_wrapper']['theme'] = [
        '#type' => 'radios',
        '#title' => $this->t('Select your theme'),
        '#options' => [],
        '#required' => TRUE,
        '#title_display' => 'invisible',
        '#default_value' => 'bartik',
      ];

      $themes = \Drupal::service('extension.list.theme')->reset()->getList();
      // Excluded themes.
      $excluded = [
        'bootstrap',
        'olivero',
        'seven',
        'claro',
        'classy',
        'stark',
        'stable',
        'stable9',
        'starterkit_theme',
        'adminimal_theme',
        'vb_subtheme'
      ];
      foreach ($themes as $filename => $theme) {
        if (in_array($filename, $excluded)) {
          continue;
        }
        $form['theme_wrapper']['theme']['#options'][$filename] = $theme->info['name'] . '<br/><div class="description">' . $theme->info['description'] . '</div>';
        if ($filename == 'starterkit') {
          $form['theme_wrapper']['theme']['#default_value'] = 'starterkit';
        }
      }

      $form['theme_wrapper']['new_theme_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Starterkit copy machine name'),
        '#description' => $this->t('Please provide a machine-name for the Starterkit copy.'),
        '#states' => [
          'visible' => [
            ':input[name="theme"]' => ['value' => 'starterkit'],
          ],
        ],
        '#machine_name' => [
          'replace_pattern' => '[^a-z_]+',
          'replace' => '_',
        ],
        '#element_validate' => [
          [static::class, 'validateMachineName'],
        ],
      ];
    }

    $form['actions'] = [
      'continue' => [
        '#type' => 'submit',
        '#value' => $this->t('Assemble and install'),
        '#button_type' => 'primary',
      ],
      '#type' => 'actions',
      '#weight' => 999,
    ];

    return $form;
  }

  /**
   * Validates a machine name.
   */
  public static function validateMachineName($element, FormStateInterface $form_state, $context) {
    $value = $element['#value'];

    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }

    // Verify that the machine name not start with any number.
    if (is_numeric($value[0])) {
      $form_state
        ->setError($element, t('The machine-readable name must start with letter.'));
    }

    // Verify that the machine name not only consists of replacement tokens.
    if (preg_match('@^' . $element['#machine_name']['replace'] . '+$@', $element['#value'])) {
      $form_state
        ->setError($element, t('The machine-readable name must contain unique characters.'));
    }

    // Verify that the machine name contains no disallowed characters.
    if (preg_match('@' . $element['#machine_name']['replace_pattern'] . '@', $value)) {
      if (!isset($element['#machine_name']['error'])) {

        // Since a hyphen is the most common alternative replacement character,
        // a corresponding validation error message is supported here.
        if ($element['#machine_name']['replace'] == '_') {
          $form_state
            ->setError($element, t('The machine-readable name must contain only lowercase letters and underscores.'));
        }
      }
      else {
        $form_state
          ->setError($element, $element['#machine_name']['error']);
      }
    }
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if (!isset($GLOBALS['install_state']['vb_profile']['extra_features'])) {
      $GLOBALS['install_state']['vb_profile']['extra_features'] = [];
    }

    if (!isset($GLOBALS['install_state']['vb_profile']['default_content'])) {
      $GLOBALS['install_state']['vb_profile']['default_content'] = [];
    }

    $values = $form_state->cleanValues()->getValues();
    // Unset theme, remaining values are modules to enable.
    unset($values['theme']);

    foreach ($values as $name => $status) {
      if ($status) {
        $GLOBALS['install_state']['vb_profile']['extra_features'][] = $name;

        // We list all default_content modules so we can uninstall them later.
        if (strpos($name, '_default_content') !== false) {
          $GLOBALS['install_state']['vb_profile']['default_content'][] = $name;
        }
      }
    }

    $GLOBALS['install_state']['vb_profile']['theme'] = $form_state->getValue('theme', 'bootstrap');

    // FIX: Only copy the Starterkit-theme when FIX.
    $fix_theme_name = $form_state->getValue('fix_theme_name');
    if (isset($fix_theme_name) && $fix_theme_name != '') {
      // Install vb_fix.
      $GLOBALS['install_state']['vb_profile']['extra_features'][] = 'vb_fix';
      $GLOBALS['install_state']['vb_profile']['new_theme_name'] = $fix_theme_name;

      $theme_path = \Drupal::service('extension.path.resolver')->getPath('theme', 'fix_starterkit');
      $this->recurseCopy(realpath($theme_path), DRUPAL_ROOT . '/themes/custom/' . $fix_theme_name, $fix_theme_name, 'fix_starterkit');
    }

    // FLEX: Only copy the Starterkit-theme when starterkit is selected and new_theme_name is not empty.
    $theme_copy = $form_state->getValue('new_theme_name');
    if (isset($theme_copy) && $theme_copy != '' && $form_state->getValue('theme') == 'starterkit') {
      $GLOBALS['install_state']['vb_profile']['new_theme_name'] = $theme_copy;
      //$GLOBALS['install_state']['vb_profile']['theme'] = $theme_copy;
      $theme_path = \Drupal::service('extension.path.resolver')->getPath('theme', 'starterkit');
      $this->recurseCopy(realpath($theme_path), DRUPAL_ROOT . '/themes/custom/' . $theme_copy, $theme_copy, 'starterkit');
    }
  }

  /**
   * Copies a directory recursively.
   * @param $src
   *   The source path.
   * @param $dst
   *   The destination path.
   * @param $theme_copy
   *   The new theme name.
   * @param $replace
   *   The string which should be replaced in the new theme-files.
   */
  protected function recurseCopy($src, $dst, $theme_copy, $replace) {
    $dir = opendir($src);
    mkdir($dst);

    while(false !== ( $file = readdir($dir)) ) {
      if (($file != '.' ) && ($file != '..' )) {
        if (is_dir($src . '/' . $file) ) {
          $this->recurseCopy($src . '/' . $file, $dst . '/' . $file, $theme_copy, $replace);
        }
        else {
          // Copy the file.
          copy($src . '/' . $file,$dst . '/' . $file);

          // Open file, and replace 'starterkit' or 'fix_starterkit' in file_content with new theme-name.
          $file_contents = file_get_contents($dst . '/' . $file);
          $file_contents = str_replace($replace, $theme_copy, $file_contents);
          // Replace custom client theme name.
          $file_contents = str_replace('VB FIX Starterkit', 'VB FIX Theme - ' . $theme_copy, $file_contents);
          file_put_contents($dst . '/' . $file, $file_contents);

          // Rename if needed.
          if (strpos($dst . '/' . $file, $replace) !== false) {
            $old_name = $dst . '/' . $file;
            $new_name = str_replace($replace, $theme_copy, $old_name);
            rename($old_name, $new_name);
          }
        }
      }
    }

    closedir($dir);
  }

  /**
   * Inserts a key/value pair into an array before given index.
   *
   * @param array $array
   *   The array to work on.
   * @param string $key
   *   The key to insert.
   * @param mixed $value
   *   The value to insert.
   * @param int $index
   *   The index to insert before.
   *
   * @return array
   *   The original array with the new value added before the index.
   */
  protected function insertBeforeIndex(array $array, $key, $value, $index) {
    $array = array_slice($array, 0, $index, TRUE) +
      [$key => $value] +
      array_slice($array, $index, count($array) - $index, TRUE);

    return $array;
  }

}
